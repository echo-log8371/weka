/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package weka;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Test class for testing the five algorithms in the mini project
 * in this directory. Run from the command line with:
 * <p>
 * java weka.MiniProjectTests
 *
 * @author Team Echo
 * @version $Revision$
 */
public class MiniProjectTests extends TestSuite {

  public static Test suite() {
    TestSuite suite = new TestSuite();

    // associators
    suite.addTest(new TestSuite(weka.associations.FilteredAssociatorTest.class));

    // classifiers
    suite.addTest(new TestSuite(weka.classifiers.functions.LinearRegressionTest.class));
    suite.addTest(new TestSuite(weka.classifiers.functions.GaussianProcessesTest.class));

    // clusterers
    suite.addTest(new TestSuite(weka.clusterers.EMTest.class));
    suite.addTest(new TestSuite(weka.clusterers.CanopyTest.class));
    suite.addTest(new TestSuite(weka.clusterers.CobwebTest.class));

    return suite;
  }

  public static void main(String[] args) {
    junit.textui.TestRunner.run(suite());
  }
}
